try:
    SideA = int(input("Введите сторону А треугольника:"))
    SideB = int(input("Введите сторону B треугольника:"))
    SideC = int(input("Введите сторону C треугольника:"))
    if (SideA + SideB) > SideC and (SideB + SideC) > SideA and (SideC + SideA) > SideB:
        print("Треугольник существует.")
        Perimetr = SideA + SideB + SideC
        print("Его периметр: ", Perimetr)
    else:
        print("Это не треугольник.")
except ValueError():    
    print("Это не число...")